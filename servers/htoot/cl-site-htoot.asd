;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-htoot
  :name "cl-site-htoot"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for Hunchentoot"
  :depends-on (:cl-site :hunchentoot) 
  :serial t
  :components ((:file "package")
	       (:file "publish")))

