;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

(defparameter *LAYOUT-DIR* (asdf:system-relative-pathname :cl-site "layout/")
  "Pathname for location of all layout files (including templates).")

(defparameter *templates-dir* (merge-pathnames #P"templates/" *layout-dir*)
  "Pathname for location of template files.")

(defparameter *OUTPUT-DIR* (asdf:system-relative-pathname :cl-site "output/")
  "Pathname where the generated output should be written to.")

;; @TO-DO: store layout name as global, generate js/css pathnames from it. 
;; (More template-related variables may go here in the future)
(defparameter *DEFAULT-PAGE-TEMPLATE* "layout_2018.html"
  "Default template to use unless another is explicitly specified.")

;; General static files - images etc
(defparameter *STATIC-DIR* (asdf:system-relative-pathname :cl-site "content/static/")
  "Pathname for location of static files.")

;; The only REQUIRED field is :content which must be the filename of the page content
;; The :content field will NOT be sent to the template.
;; @TO-DO: store fields within content files and parse them out.
(defparameter *PAGES-DIR* (asdf:system-relative-pathname :cl-site "content/")
  "Pathname for location of page content.")
(defparameter *PRIVATE-KEYS* '(:slug :content))


;; Initialize global context (will be appended to all individual page contexts)
;; Used to store things like stylesheets, etc...
(defparameter *GLOBAL-CONTEXT* ())

(defparameter *computed-page-content* nil
  "Used as dynamically bound variable in preprocess-lisp-pages, to
  capture output when compile/loading any lispy html content files
  wrapped with calls to add-content. Global value should always be
  nil.")

(defvar *paserve-port* 8008)
;; FLAG add different ports for other webservers when activated
