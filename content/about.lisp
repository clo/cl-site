(in-package :cl-site-content)


(add-content
 :headers-plist (list :|title| "Common-Lisp.net | About")
 :html-string
 (with-cl-who-string (:indent t)
   (:h1 ((:i :class "far fa-question-circle")) " About Common-Lisp.net")

   (wmd "
  
Common-Lisp.net provides hosting for projects and user groups related to the programming language Common Lisp.


The site is run under the governance of the [Common Lisp
Foundation](http://cl.foundation) and maintained mostly by a
committee of volunteers.  [Please consider donating](/contribute).


Its source is hosted on
[gitlab.common-lisp.net](https://gitlab.common-lisp.net/clo/cl-site).


If you want to request a project, read [this
document](/project-intro).


If you have questions, suggestions or comments about this site or you
simply wish to reach the current maintainers, email [this mailing
list](http://mailman.common-lisp.net/cgi-bin/mailman/listinfo/clo-devel).

The documentation, procedures, feature requests etc are located in our
Trac instance: [clo's Trac](http://trac.common-lisp.net/clo).")

   (:h2 "Maintainance")

   (wmd "

Since many years [Common-Lisp.net](/) has been run by a number of
volunteers.")
  
   (:h3 "Currently active maintainers")

   (wmd "

* Mark Evenson
* Erik Huelsmann
* Mariano Montone")

   (:h3 "Previous maintainers")

   (wmd "

* Erik Enge
* Drew Crampsie
* Hans H&uuml;bner
* C. Yang
* Mario S. Mommer")))



