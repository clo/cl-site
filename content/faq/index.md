title: Frequently asked questions
---

# FAQ - Common-Lisp.Net Frequently asked questions

{table-of-contents :depth 3 :start 2 :label "Table of contents" }

## GitLab

### Configure e-mail on push

GitLab supports sending an e-mail upon pushing
commits by project members.  In order to use the functionality,
please follow the steps outlined in
[configuration guide](/faq/emailonpush).

### Deploy project pages

Use GitLab Pages to deploy your project pages
to `https://<project>.common-lisp.dev/`
using our [deployment instructions](/faq/using-gitlab-deploy-project-pages).

### Test a project

Use GitLab CI to test against multiple CL implementations using
our [GitLab CI for CL intro](/faq/using-gitlab-ci-for-cl).

### Populate a project page with content from multiple repos

Use GitLab CI to create multi-project pipelines using our
[multi-project CI instructions](/faq/using-gitlab-ci-multi-project-pipelines).
